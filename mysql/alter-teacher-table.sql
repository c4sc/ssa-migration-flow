ALTER TABLE STEPS_TEACHER
ADD COLUMN `TCHCD1` varchar(16) NULL DEFAULT NULL,
ADD COLUMN `PENNO` int(6) NULL,
ADD COLUMN `DESIGNATION` int(2) NULL,
ADD COLUMN `ROLE` int(2) NULL,
ADD COLUMN `STATUS` int(1) NULL DEFAULT '1';

DELETE FROM STEPS_TEACHER WHERE AC_YEAR != '2014-15'
DELETE FROM STEPS_TEACHER WHERE AC_YEAR != '2014-15'

UPDATE STEPS_TEACHER SET AC_YEAR = '2015-16';
UPDATE STEPS_TEACHER SET TCHCD1 = CONCAT(`SCHCD`, SUBSTR(`AC_YEAR`, 3, 2), `SLNO`);
UPDATE STEPS_TEACHER SET PENNO = SUPVAR9;

ALTER TABLE `STEPS_TEACHER` ADD PRIMARY KEY ( `TCHCD1` ) ;

select CONCAT(`SCHCD`, SUBSTR(`AC_YEAR`, 3, 2), `SLNO`) as tchcd1, ac_year from STEPS_TEACHER;




------------------------------------------


ALTER TABLE STEPS_TEACHER DROP `TCHCD1`,
ALTER TABLE STEPS_TEACHER DROP `PENNO`;
ALTER TABLE STEPS_TEACHER DROP `DESIGNATION`;
ALTER TABLE STEPS_TEACHER DROP `ROLE`;
ALTER TABLE STEPS_TEACHER DROP `STATUS`;
