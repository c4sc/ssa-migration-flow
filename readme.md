first: oracle to mysql
----------------------
1. get .sql of oracle db from ssa guys

2. vim -s filter.keys oracle-exported-sql-dump.sql
    ; removes all unwanted lines, and ddl from the sql dump

3. create intermediate database in mysql to import this data
    * the oracle-STEPS_TEACHER table is not identical to that of online_mysql_db, but it wont be a problem for insertion

4. import the oracle dumped sql into that intermediate db.

5. delete unwanted teachers, by changing AC_YEAR value appropriately. !warning... the query shown here is not the right one!!
    DELETE FROM STEPS_TEACHER WHERE AC_YEAR NOT IN ('2013-14', '2014-15', '2015-16');

6. alter table STEPS_TEACHER to make it identical to online_mysql_db
    * missing fields in oracle:
        DESIGNATION
        PENNO
        ROLE
        STATUS
        TCHCD1 (!! most important).

7. insert TCHCD1 value by concatenating the SCHCD, SLNO fields.

    DELETE FROM STEPS_TEACHER WHERE AC_YEAR != '2014-15'
    DELETE FROM STEPS_TEACHER WHERE AC_YEAR != '2014-15'
    UPDATE STEPS_TEACHER SET AC_YEAR = '2015-16';
    UPDATE STEPS_TEACHER SET TCHCD1 = CONCAT(`SCHCD`, SUBSTR(`AC_YEAR`, 3, 2), `SLNO`);
    UPDATE STEPS_TEACHER SET PENNO = SUPVAR9;

8. fix the dob issue using following query
    update STEPS_TEACHER set DOB = DATE_SUB(`DOB`, INTERVAL '100' YEAR) where EXTRACT(YEAR FROM `DOB`) > 2000;

9. merge it with online_mysql_db
    * to merge, truncate the tables which needs to be inserted data from intermediate-db(mysql)
        - mysql/truncate.sql

10. dump the merged db, and import into online server
    - mysql/dump-tables.sh   ; change db name appropriately

second: mysql to oracle
-----------------------

1. get a dump from online_mysql_db after the data entry is done.
2. then copy it into a intermediate_mysql_db
3. drop the tables which is not required by oracle
    /* write a query for that */
4. remove special character from intermediate_mysql_db
5. then dump intermediate_mysql_db using phpmyadmin with following options (I am not able to find a command to do this)
    * select the tables which is required to be inserted into oracle-db
    * uncheck 'display comments'
    * only 'data' needs to be exported
    * Enclose table and column names
    * uncheck 'Truncate table before insert'
    * function to use when dumping data 'INSERT'
    * Syntax to use when inserting data: 'include column names in every INSERT statement'
    * uncheck 'Dump binary columns in hexadecimal notation'
    * uncheck 'Dump TIMESTAMP columns in UTC'
6. run following replacements on intermediate_mysql_db_dump.sql
    - escaping special characters like apostrophy, &
    - remove comments
    - change date value into this
        '1970-01-01 00:00:00'  --> TO_DATE('1970-01-01', 'YYYY-MM-DD');
    $ echo 'set define off;' | cat - intermediate_mysql_db_dump.sql > /tmp/out && mv /tmp/out intermediate_mysql_db_dump.sql
        ; insert a line at the beginning of file
7. create a schema/user in oracle
8. get ddl of all oracle tables from the .sql given by ssa
    * for now, I do have it in a seperate ddl.sql file
9. import .sql of oracle db(fromm ssa) into the schema oracle_db
10. import intermediate_mysql_db_dump.sql into it
