:syn off
:%s/\r/\r/g
:%s/^  CREATE TABLE "DISE2K_.*"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_*"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "CHK_TABLE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "RTE_COMPLAINT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "SCH_TEMP"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "SCH_TEMP1"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_AUTH"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_CWSNCHILDDB"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_DATE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_DBINFO"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_DCF2PRINT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_DISTPHASE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_PIVOT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_POP4COMPRATE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_POPULATION_ALL"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_PRINTDCF"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_QUERY"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_RMSASUBJECTS"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_ANAREPBYMCR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_ANAREPINFO"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_ENRBYMCR_SCR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_EREPBYMCR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_REPINFO"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_SCHGRADE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_SREPBYMCR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_R_TREPBYMCR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_SCHBYCATMGT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_SCHGRADING"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TEACHER_RTE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TEHSIL"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TOTDISABILITY"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TOTENROLMENT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TOTPINCENTIVES"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TOTUINCENTIVES"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
---
:%s/^  CREATE TABLE "STEPS_BLDSTATUS"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_BNDRYWALL"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_CASTE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_MEDINSTR"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_RESITYPE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_SCHCAT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_SCHMGT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHAQUAL"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHCASTE"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHCAT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHCLSTAUGHT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHPQUAL"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_TCHSUBTAUGHT"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:%s/^  CREATE TABLE "STEPS_WATER"\(\_s.["|,|A-z|0-9|\(|\)]*\)* ;//
:g/^--/d
:g/^$/d
:wq ddl.sql
:q!
